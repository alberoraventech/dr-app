import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username = new FormControl('');
  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  getCodeBoxElement(index: any): any {
    return document.getElementById('codeBox' + index);
  }
  onKeyUpEvent(index: any, event: any) {
    const eventCode = event.which || event.keyCode;
    if (this.getCodeBoxElement(index).value.length === 1) {
     if (index !== 4) {
      this.getCodeBoxElement(index+ 1).focus();
      console.log(event.target.value);
     } else {
      this.getCodeBoxElement(index).blur();
      // Submit code
      console.log('submit code ');
     }
    }
    if (eventCode === 8 && index !== 1) {
     this.getCodeBoxElement(index - 1).focus();
    }
  }
  onFocusEvent(index: any) {
    for (let item = 1; item < index; item++) {
     const currentElement = this.getCodeBoxElement(item);
     if (!currentElement.value) {
        currentElement.focus();
        break;
     }
    }
  }
  onSubmit() {
    let password: any = document.getElementsByClassName('password');

    let temp = {
      deviceToken:"",
      deviceType:"mobileweb",
      logintype:"guest",
      mobile_number: this.username.value,
      password: [...password].map(res => res.value).join('')
    };
    
    this.http.post(environment.apiBaseUrl+'doctor/signin', temp).subscribe((res:any) =>{
      console.log(res);
      if (res.status) {
        this.router.navigateByUrl('pages/home');
      }
    })
  }
}
